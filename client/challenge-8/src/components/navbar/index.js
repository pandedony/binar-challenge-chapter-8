import React, { Component } from "react";

function Navbar(){
  return (
    <nav className="navbar navbar-light bg-light">
      <div className="container">
        <a className="navbar-brand" href="#">
          Home
        </a>
      </div>
    </nav>
  );
}

export default Navbar;