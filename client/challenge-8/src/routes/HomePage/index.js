import { Container, Form, Button, Table } from "react-bootstrap";
import { Component } from "react";

export default class HomePage extends Component {
  documentData;

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.state = {
      id: new Date(),
      name: "",
      email: "",
      password: "",
      experience: "",
      level: "",
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // on form submit...
  handleFormSubmit(e) {
    e.preventDefault();
    let data = [];
    if (localStorage.getItem("document")) {
      data = JSON.parse(localStorage.getItem("document"));
      data.push(this.state);
      localStorage.setItem("document", JSON.stringify(data));
    } else {
      data.push(this.state);
      localStorage.setItem("document", JSON.stringify([this.state]));
    }
    this.setState({ [e.target.name]: e.target.value });
  }

  renderTableData() {
    let data = [];
    if (localStorage.getItem("document")) {
      data = JSON.parse(localStorage.getItem("document"));
    }

    return (
      <Table striped>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Experience</th>
            <th>Level</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => {
            return (
              <tr key={index}>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>{item.password}</td>
                <td>{item.experience}</td>
                <td>{item.level}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <Container className="mt-3">
          <Form onSubmit={this.handleFormSubmit}>
            <Form.Group className="mb-3">
              <Form.Label>Player Username</Form.Label>
              <Form.Control type="text" name="name" className="form-control" value={this.state.name} onChange={this.handleChange} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Email</Form.Label>
              <Form.Control type="text" name="email" className="form-control" value={this.state.email} onChange={this.handleChange} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" name="password" className="form-control" value={this.state.password} onChange={this.handleChange} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Experience</Form.Label>
              <Form.Select aria-label="Default select example" value={this.selected} name="experience" onChange={this.handleChange}>
                <option>Open this select menu</option>
                <option value="Beginner">Beginner</option>
                <option value="Advance">Advance</option>
                <option value="Expert">Expert</option>
              </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Level</Form.Label>
              <Form.Select aria-label="Default select example" value={this.selected} name="level" onChange={this.handleChange}>
                <option>Open this select menu</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </Form.Select>
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Container>

        <Container className="mt-3">
          <h1>Detail Player Data</h1>
          <div>{`Player Name: ${this.state.name}`}</div>
          <div>{`Email: ${this.state.email}`}</div>
          <div>{`Password Lenght: ${this.state.password.length}`}</div>
          <div>{`Experience: ${this.state.experience}`}</div>
          <div>{`Level: ${this.state.level}`}</div>
        </Container>

        <Container className="mt-3">
          <h1>Player List Data</h1>;{this.renderTableData()}
        </Container>
      </div>
    );
  }
}
