import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import NavBar from "./components/navbar";
import HomePage from "./routes/HomePage";

function App() {
  return (
    <Router>
      <div>
        <NavBar />
        <Routes>
          {/* <Route path="/editplayer" element={<EditPage />} /> */}
          {/* <Route path="/player" element={<SearchPage />} /> */}
          <Route path="/" element={<HomePage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
